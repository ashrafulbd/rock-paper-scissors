let userScore = 0;
let compScore = 0;

const userScore_span = document.getElementById("user-score");
const compScore_span = document.getElementById("comp-score");

const scoreBorard_div = document.querySelector(".score-board");
const result_p = document.querySelector(".result > p");

const rock_div = document.getElementById("r");
const paper_div = document.getElementById("p");
const scissors_div = document.getElementById("s");

const userSmallView = "user".fontsize(3).sub();
const compSmallView = "comp".fontsize(3).sub();

const optionDict = {
    "r": "Rock",
    "p": "Paper",
    "s": "Scissors"
};

main();

function main() {

    rock_div.addEventListener('click', function() {
        game("r");
    })
    paper_div.addEventListener('click', function() {
        game("p");
    })
    scissors_div.addEventListener('click', function() {
        game("s");
    })
}

function showResult() {
    userScore_span.innerHTML = userScore
    compScore_span.innerHTML = compScore
}

function win(userInput, computerInput) {
    userScore++;
    showResult();
    result_p.innerHTML = `${optionDict[userInput]}${userSmallView} beats ${optionDict[computerInput]}${compSmallView}. You win!`
}

function lose(userInput, computerInput) {
    compScore++;
    showResult();
    result_p.innerHTML = `${optionDict[userInput]}${userSmallView} loses ${optionDict[computerInput]}${compSmallView}. You lose!`
}

function draw(userInput, computerInput) {
    result_p.innerHTML = `${optionDict[userInput]}${userSmallView} draws ${optionDict[computerInput]}${compSmallView}. Draw!`
}


function glowInput(userInput, computerInput) {

    if (userInput === computerInput) {
        document.getElementById(userInput).classList.add('gray-glow');
        setTimeout(function() { document.getElementById(userInput).classList.remove('gray-glow'); }, 300)

    } else {
        document.getElementById(userInput).classList.add('green-glow');
        document.getElementById(computerInput).classList.add('red-glow');
        setTimeout(function() { document.getElementById(userInput).classList.remove('green-glow'); }, 300)
        setTimeout(function() { document.getElementById(computerInput).classList.remove('red-glow'); }, 300)
    }

}

function game(userInput) {

    const computerInput = getComputerInput()

    glowInput(userInput, computerInput);

    switch (userInput + computerInput) {
        case "rs":
        case "pr":
        case "sp":
            win(userInput, computerInput);
            break;

        case "rp":
        case "ps":
        case "sr":
            lose(userInput, computerInput);
            break;

        case "rr":
        case "pp":
        case "ss":
            draw(userInput, computerInput);
            break;
    }

}

function getComputerInput() {
    const randomNum = Math.floor(Math.random() * Math.floor(3));
    return Object.keys(optionDict)[randomNum]
}